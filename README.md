GADEGO published games by Erik Olson and Graig Donini in the 80s.

## Bubble Plane

In the game's development, there was first an Extended Basic version. The gameplay
was translated into assembly, but it still loads from Extended Basic.

You can play it in Classic99 or js99er.net. Load the file bubble_plane.dsk
into DSK1 and go into Extended Basic.  

Disk contents:

`GADEGO    :    247 used  41 free   72 KB  1S/1D 32T  9 S/T
BPSCRS      161  DIS/FIX 255  40960 B 160 recs                            
GAME         77  INT/VAR 254  19306 B  76 recs                            
HIGH          3  INT/FIX 80     512 B   6 recs                            
LOAD          2  PROGRAM         91 B                                     
RESET         2  PROGRAM        179 B            `

The program RESET will clear the high scores file.

Instructions:

Fly the Bubble Plane. Get a feel for how the plane flies.

Use the arrow keys or joystick. 

Reach the enemy base and guide Bubble Plane onto the runway.
